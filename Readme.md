Prerequisites

You should have NodeJS LTS installed on your machine. Then if you don't have expo cli installed on your machine you can install it globally by using the following command:

npm install -g expo-cli

Installation

1. npm install

Running the app

1. Install expo client on your phone
2. npm start - run this on terminal it will open expo devtools on your browser
3. Scan the QR code from devtools on your phone using expo client app
